## TESTE DESENVOLVEDOR JR NETSHOES

### 1º TESTE

APIs endpoints

GET http://localhost:9000/api/listar --Lista todos clientes

GET http://localhost:9000/api/listar/{cpf} --Lista um cliente por CPF

POST http://localhost:9000/api/cadastrar --Cadastra um novo cliente

DELETE http://localhost:9000/api/apagar/{id) --Remove um cliente por ID

H2 Console http://localhost:9000/h2-console --Banco de dados utilizado

### 2º TESTE
```
public class formatarTelefone {

	// Método
	public static String formataTelefone(String numero) {
		if (numero != null && numero.length() == 10) {
			try {
				MaskFormatter mascara = new MaskFormatter("(##) ####-####");
				JFormattedTextField numeroFormatar = new JFormattedTextField(mascara);
				numeroFormatar.setText(numero);

				return ("Telefone: " + numeroFormatar.getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return ("Algo deu errado");
			}
		}if (numero != null && numero.length() == 11) {
			try {
				MaskFormatter mascara = new MaskFormatter("(##) ####-####");
				JFormattedTextField numeroFormatar = new JFormattedTextField(mascara);
				numeroFormatar.setText(numero);

				return ("Celular: " + numeroFormatar.getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return ("Deu ruim");
			}
		}
		return "Digite o DD seguido do número para formatar!";
	}

}

```
### 3º TESTE

```
SELECT COD_PRODUTO,DESC_PRODUTO, COUNT(COD_PEDIDO) AS QUANTIDADE FROM ITENS_PEDIDOS
GROUP BY DESC_PRODUTO,COD_PRODUTO HAVING COUNT(*) > 1
```

### 4º TESTE

```
db.produto.find({"descricao": {$regex: /Alemanha/}})
```


