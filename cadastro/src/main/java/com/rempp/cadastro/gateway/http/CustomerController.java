package com.rempp.cadastro.gateway.http;

import com.rempp.cadastro.model.Customer;
import com.rempp.cadastro.usecase.CustomerCrud;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@SuppressWarnings("unused")
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/customer")
public class CustomerController {

    private final CustomerCrud customerCrud;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Customer> getAllCustomers() {
        return customerCrud.findAll();
    }

    @GetMapping("{cpf}")
    @ResponseStatus(HttpStatus.OK)
    public Customer findByCPF(@PathVariable(value = "cpf") String cpf) {
        return customerCrud.findById(cpf);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer saveCustomer(@Valid @RequestBody Customer customer) {
        return customerCrud.create(customer);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateCustomer(@Valid @RequestBody Customer customer) {
        customerCrud.update(customer);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") String id) {
        customerCrud.deleteById(id);
    }
}