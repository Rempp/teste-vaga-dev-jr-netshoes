package com.rempp.cadastro.gateway;

import com.rempp.cadastro.model.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerGateway {
    List<Customer> findAll();

    Optional<Customer> findById(String cpf);

    Customer save(Customer customer);

    void deleteById(String id);
}