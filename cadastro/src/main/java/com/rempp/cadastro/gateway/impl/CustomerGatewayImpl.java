package com.rempp.cadastro.gateway.impl;

import com.rempp.cadastro.gateway.CustomerGateway;
import com.rempp.cadastro.gateway.repository.CustomerRepository;
import com.rempp.cadastro.model.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CustomerGatewayImpl implements CustomerGateway {

    private final CustomerRepository customerRepository;

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Optional<Customer> findById(String cpf) {
        return customerRepository.findById(cpf);
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public void deleteById(String id) {
        customerRepository.deleteById(id);
    }
}