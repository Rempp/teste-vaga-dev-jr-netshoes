package com.rempp.cadastro.model;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Customer {

    @Id
    @NotNull(message = "Forneça um CPF válido")
    @CPF(message = "Forneça um CPF válido")
    @Column(unique = true)
    private String cpf;

    @NotNull(message = "Forneça um Nome")
    private String name;

    @NotNull(message = "Forneça um E-mail")
    @javax.validation.constraints.Email(message = "Forneça um E-mail válido")
    private String email;
}