package com.rempp.cadastro.usecase;

import com.rempp.cadastro.gateway.CustomerGateway;
import com.rempp.cadastro.model.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class CustomerCrud {

    private final CustomerGateway customerGateway;

    public List<Customer> findAll() {
        return customerGateway.findAll();
    }

    public Customer findById(String cpf) {
        return customerGateway.findById(cpf).orElse(null);
    }

    public Customer create(Customer customer) {
        if (!Objects.nonNull(findById(customer.getCpf()))) {
            return customerGateway.save(customer);
        } else {
            throw new RuntimeException("Resource was already created");
        }
    }

    public void deleteById(String id) {
        customerGateway.deleteById(id);
    }

    public void update(Customer customer) {
        if (Objects.nonNull(findById(customer.getCpf()))) {
            customerGateway.save(customer);
        } else {
            throw new RuntimeException("Resource Not Found");
        }
    }
}